using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace QuizShowApplication
{
    public partial class Register : Form
    {

        SqlConnection connect = new SqlConnection(@"Server=DESKTOP-C4DUS3\SQLEXPRESS; Initial Catalog=quizapp;Integrated Security=True");
        public Register()
        {
            InitializeComponent();

        }


        private void createAccountBtn_Click(object sender, EventArgs e)
        {


            if (usernameTxt.Text == "" || passwordTxt.Text == "" || emailTxt.Text == "" || ageTxt.Text == "" || phoneNumberTxt.Text == "")
            {
                MessageBox.Show("You should fill all the blanks.");
            }
            else
            {
                String username;
                string password;
                string email;
                int age;
                long phonenumber;

                //Initializing variables
                username = usernameTxt.Text;
                password = passwordTxt.Text;
                email = emailTxt.Text;
                age = Convert.ToInt32(ageTxt.Text);
                phonenumber = Convert.ToInt64(phoneNumberTxt);

                //Started connection db
                connect.Open();
                SqlCommand usernameCheck = new SqlCommand("SELECT * FROM kullanici WHERE kAdi='" + username + "'", connect);
                SqlDataReader check = usernameCheck.ExecuteReader();
                if (check.Read())
                {
                    MessageBox.Show("This name is already taken. Please choose different name.");
                }
                else
                {
                    //Starting store data
                    check.Close();
                    SqlCommand register = new SqlCommand("INSERT INTO kullanici(username, password, email, age, phonenumber) VALUES('" + username + "', '" + password + "', '" + email + "', '" + age + "' ,'"+ phonenumber +"' )", connect);
                    register.ExecuteNonQuery();
                    MessageBox.Show("Succesfull! You are directing to the form!");

                    Register r = new Register();
                    r.Show();
                    this.Hide();
                }
            }

            
        }
        private void ageTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Just Accepts Characters
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void phoneNumberTxt_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void Register_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Just Accepts Characters
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }
    }
}

        